{
    "add_repo": {
        "intro": "Escaneie códigos QR de repositórios que você deseja adicionar de dispositivos ou sites onde eles podem ser encontrados.",
        "no_internet": "Sem internet? Sem problemas. Saiba <a href=\"../swap/\">Como enviar e receber aplicativos off-line.</a>",
        "open_repo_link": {
            "description": "Se você enviar um link de repositório por e-mail, mídia social ou SMS, adicione a capacidade de instalar os aplicativos no F-Droid abrindo-o.",
            "short_summary": "Abrir o Link de Repositório",
            "steps": {
                "step_1": "Descarregue e instale o app do F-Droid no seu aparelho Android.",
                "step_2": "Clique no link e abra-o em um navegador da Web no seu aparelho.",
                "step_3": "Quando você tiver o link aberto, toque no botão \"Adicionar ao F-Droid\".",
                "step_4": "Toque no botão \"I have F-Droid\". O aplicativo F-Droid será aberto.",
                "step_5": "\"Adicionar novo repositório\" é mostrado com o endereço do repositório já preenchido. Toque em \"OK\" para adicioná-lo.",
                "step_6": "Seu novo repositório será adicionado. Para ver uma lista de seus repositórios, abra \"Configurações\". Selecione \"Repositórios\".",
                "step_7": "Toque em um repositório para ver seu conteúdo.",
                "step_8": "Selecione os aplicativos que você gostaria de ter e instale-os no seu dispositivo."
            },
            "title": "Abra o Link de Repositório no Seu Dispositivo"
        },
        "overview": {
            "summary": "Para obter mais, adicione um repositório ao F-Droid. Um repositório é uma coleção de conteúdo selecionado. Você pode descobrir sobre os repositórios das pessoas diretamente delas. Os desenvolvedores geralmente se conectam ao repositório F-Droid de seu site. Frequentemente, os instrutores compartilham links para repositórios por e-mail, Facebook ou Twitter.",
            "title": "Obtenha mais conteúdo."
        },
        "scan_qr_code": {
            "description": "Escaneie códigos QR de repositórios que você deseja adicionar de dispositivos ou sites onde eles podem ser encontrados.",
            "related_tutorial": "Talvez você também esteja interessado em: <a href=\"../swap/\">Como Enviar e Receber Aplicativos Off-line</a>",
            "short_summary": "Escanear Código QR",
            "steps": {
                "step_1": "Uma vez que o código QR para o repositório seja exibido, digitalize o código QR usando um aplicativo scanner de QR.",
                "step_2": "Toque no link. Se tiver a opção, abra o link usando o F-Droid. Em seguida, siga as instruções. Se isso funcionar, está feito! Se não, avance para o próximo passo.",
                "step_3": "Se você não tiver a opção de abrir o link com o F-Droid, feche o aplicativo scanner de QR e abra o aplicativo F-Droid.",
                "step_4": "No F-Droid, toque em \"Configurações\". Em \"Meus aplicativos\", selecione \"Repositórios\".",
                "step_5": "No ecrã Repositórios, toque no ícone \"+\" no canto superior.",
                "step_6": "O endereço do repositório será preenchido automaticamente. Toque em \"Ativar\". Observação: o repositório será salvo em Repositórios do F-Droid, mas nenhum aplicativo será instalado até você escolher e instalar manualmente."
            },
            "title": "Escaneie um Código QR de Outro Dispositivo"
        },
        "title": "Como Adicionar um Repositório ao F-Droid"
    },
    "create_repo": {
        "customize_repo": {
            "description": "Você pode editar o aplicativo e os resumos de ficheiros para torná-los compreensíveis para seu público-alvo. Você pode editar o idioma especificando o país. Em seguida, compartilhe o repositório sem problemas para os participantes antes de conferências e treinamentos.",
            "steps": {
                "step_1": "Clique em um item para abrir seus detalhes.",
                "step_2": "Clique \"Editar\".",
                "step_3": "Personalize o conteúdo para se adequar ao seu público-alvo. Nota: As alterações que você fizer serão feitas apenas para o seu repositório. Elas não serão atualizadas para o fonte original. Por favor, use respeito ao personalizar as informações de conteúdo que não é seu.",
                "step_4": "Todas as suas alterações são salvas automaticamente."
            },
            "title": "Personalize Seu Repositório"
        },
        "download_and_install": {
            "steps": {
                "step_1": "Baixe e instale o aplicativo web Repomaker no seu computador. Consulte o <a href=\"https://gitlab.com/fdroid/repomaker/blob/master/README.md#installation\">guia de instalação</a> para obter mais informações sobre como fazer isso.",
                "step_2": "Inicie o aplicativo web Repomaker. Inscreva-se ou faça o login para começar.",
                "step_3": "Crie um novo repositório e nomeie-o de acordo.",
                "step_4": "Escolha adicionar aplicativos da galeria e dos repositórios disponíveis ou envie seus próprios ficheiros. Clique no botão \"Adicionar da Galeria\" para escolher o conteúdo prontamente disponível.",
                "step_5": "Clique no botão \"Adicionar\" ao lado dos itens que você deseja adicionar. Quando terminar, clique na seta \"voltar\" para retornar ao seu repositório.",
                "step_6": "Seus aplicativos serão exibidos em sua visualização de conteúdo do repositório."
            },
            "title": "Baixe e instale o Repomaker para começar a criar"
        },
        "intro": "Não consegue encontrar o que você está procurando? Descarregue o aplicativo web Repomaker no seu computador para criar sua própria coleção personalizada de aplicativos e ficheiros.",
        "overview": {
            "summary": "Crie seu próprio repositório personalizado de aplicativos. Use o F-Droid para distribuir conteúdo. Os instrutores usam repositórios para compartilhar facilmente uma coleção de recursos e aplicativos com os participantes em um treinamento quando não há conexão de internet confiável. Os desenvolvedores usam repositórios para distribuir seus aplicativos para públicos de nicho.",
            "title": "Adicione seus próprios aplicativos e ficheiros."
        },
        "related_tutorial": "Você pode também estar interessado em: <a href='../add-repo/'>Como Adicionar um Repositório ao F-Droid</a>",
        "share_repo": {
            "steps": {
                "step_1": "Com o repositório que você deseja compartilhar aberto, clique em \"Compartilhar\" no menu. Nota: você precisa configurar um local para armazenar seu repositório antes de compartilhá-lo. Instruções sobre como fazer isso estão na Etapa 2.",
                "step_2": "Você será solicitado a configurar o armazenamento para publicar seu repositório e antes de compartilhá-lo. Se você não vir esta página, continue na Etapa 4.",
                "step_3": "Escolha a opção de armazenamento que melhor atenda às suas necessidades. Depois de ter o armazenamento, você está pronto para compartilhar! Volte para \"Compartilhar\".",
                "step_4": "Escolha copiar o link e enviá-lo diretamente para as pessoas por e-mail ou texto, ou compartilhar seu repositório no Facebook ou Twitter. Quando as pessoas abrirem o link para o seu repositório, elas precisarão adicioná-lo a um dispositivo Android para acessar os aplicativos no repositório. Consulte <a href='../add-repo/'>Como Adicionar um Repoitório ao F-Droid</a> para mais informações.",
                "step_5": "A opção \"Ver Repositório\" permite ver o que você acabou de publicar ou compartilhar."
            },
            "title": "Personalize Seu Repositório"
        },
        "title": "Como Criar um Repositório",
        "update_repo": {
            "steps": {
                "step_1": "Se você adicionar aplicativos da galeria, eles serão atualizados automaticamente quando a fonte adicionar novas versões. Para os ficheiros que você enviou, você pode arrastar e soltar as versões atualizadas na visualização do repositório.",
                "step_2": "Como alternativa, você pode abrir um único item para adicionar uma nova versão. Selecione editar.",
                "step_3": "Role para baixo para as versões. Em seguida, adicione uma nova versão na seção \"Histórico de Versões\".",
                "step_4": "Em seguida, adicione uma nova versão na seção \"Histórico de Versões\"."
            },
            "title": "Atualize Seu Repositório"
        }
    },
    "misc": {
        "option_x_description": "Opção [number]: [description]",
        "overview": {
            "summary": "O F-Droid é uma loja de aplicativos independente e de origem comunitária para Android, totalmente livre e de código aberto. No F-Droid, você pode navegar por mais de 1.200 aplicativos de código aberto, pesquisar e instalar aplicativos de repositórios criados ou criar seu próprio repositório. Abra f-droid.org no seu dispositivo Android para baixar o aplicativo e começar!",
            "title": "O Que Você Pode Fazer com o F-Droid",
            "tutorial_x_title": "Tutorial [number]: [title]"
        },
        "step_x": "Etapa [number]",
        "step_x_description": "Etapa [number]: [description]"
    },
    "swap": {
        "all_devices_need_fdroid": "Todos os dispositivos precisam do F-Droid baixado e instalado antes de começar. Todos os dispositivos devem seguir as etapas abaixo.",
        "intro": "Sem internet? Sem problemas. Baixe aplicativos de pessoas perto de você.",
        "overview": {
            "summary": "Sem internet? Sem problemas. O F-Droid \"Por Perto\" (Nearby, em inglês) permite que você envie e receba conteúdo para pessoas na mesma sala.",
            "title": "Envie e receba aplicativos off-line."
        },
        "related_tutorial": "Você também pode se interessar por: <a href=\"../create-repo/\">Como Criar um Repositório</a>",
        "step_1": "Abra o F-Droid. Toque em \"Por Perto\" no menu na parte inferior do ecrã.",
        "step_10": "Para acessar seus aplicativos instalados, abra as configurações do F-Droid e selecione \"Gerenciar aplicativos instalados\" em \"Meus aplicativos\".",
        "step_2": "Toque em \"Encontrar pessoas próximas a mim\" para pesquisar dispositivos compatíveis.",
        "step_3": "Quando as pessoas próximas forem encontradas, você e o contato deverão selecionar um ao outro.",
        "step_4": "Depois de conectado, escolha os aplicativos que você deseja compartilhar com seu contato.",
        "step_5": "Toque no botão \"—>\".",
        "step_6": "Uma pessoa será solicitada a confirmar a solicitação para se conectar.",
        "step_7": "Toque no botão \"Instalar\" ao lado dos aplicativos que você deseja instalar.",
        "step_8": "Toque em \"Instalar\" mais uma vez. Em seguida, siga as instruções do Android para concluir o processo de instalação.",
        "step_9": "Quando as instalações forem concluídas, seus aplicativos estarão disponíveis na galeria de aplicativos do Android e no aplicativo do F-Droid em aplicativos instalados.",
        "title": "Como Enviar e Receber Aplicativos Off-line"
    }
}
